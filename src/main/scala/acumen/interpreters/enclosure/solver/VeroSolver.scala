package acumen.interpreters.enclosure.solver

import acumen.interpreters.enclosure.Box
import acumen.interpreters.enclosure.Field
// import acumen.interpreters.enclosure.Interval
import acumen.interpreters.enclosure.Rounding
import acumen.interpreters.enclosure.affine.UnivariateAffineEnclosure
import acumen.interpreters.enclosure.Types._
import acumen.interpreters.enclosure.Expression
import acumen.interpreters.enclosure._
import acumen.interpreters.enclosure.solver.vero.Floating
import acumen.interpreters.enclosure.solver.vero.RoundedNum
import acumen.interpreters.enclosure.solver.vero.Dif
import acumen.interpreters.enclosure.solver.vero.TaylorSolver

trait VeroSolver extends SolveIVP {

  /**
   * The Tucker solver for a given time interval (the T is fixed).
   * I would like to settle a translation of the AST field
   * into a scala function once for each field so that the
   * translation is not part of every call to solveVt.
   * A value of the right type in the trait and
   * a fast way to test for equality between ASTs?
   * Is it enough to save the AST and test for pointer equality?
   */


  type IntervalV[A]   = acumen.interpreters.enclosure.solver.vero.Interval[A]
  type Real           = acumen.interpreters.enclosure.Interval.Real
  type IntervalOfReal = IntervalV[Real]

  def solveVt(
    F: Field, // field
    T: acumen.interpreters.enclosure.Interval, // domain of t
    A: Box, // (A1,...,An), initial condition
    delta: Double, // padding 
    m: Int, // extra iterations after inclusion of iterates
    n: Int, // maximum number of iterations before inclusion of iterates
    degree: Int // number of pieces to split each initial condition interval
    )(implicit rnd: Rounding): (UnivariateAffineEnclosure, Box) = {
    
    val (xp, e) = F match {
      case Field(components) => components head   
    }
    val prec  = rnd match {
      case Rounding(p) => p
    }
    
    if((xp takeRight 2) == "''") // The eqn is x'' = f(x', x), x'(0) = x0', x(0) = x0 
      taylorSolveA(xp dropRight 2, e, T, A, prec)
    else // The eqn is x' = f(x), x(0) = x0
      taylorSolve(xp dropRight 1, e, T, A, prec)
  }

  

  def taylorSolveA(
    v : String, 
    e : Expression, 
    T : acumen.interpreters.enclosure.Interval, 
    A : Box,
    prec : Int) :  (UnivariateAffineEnclosure, Box) = null
  
  def taylorSolve(
    v : String, 
    e : Expression, 
    T : acumen.interpreters.enclosure.Interval, 
    A : Box,
    prec : Int) :  (UnivariateAffineEnclosure, Box) = { // The eqn is x' = f(x), x(0) = x0
    
    
    implicit val wRN = new acumen.interpreters.enclosure.solver.vero.Context(prec).BigDecimal2RoundedNum
    import wRN._
    
    implicit val wF = acumen.interpreters.enclosure.solver.vero.BigDecimalInstances.BigDecimal2Floating
    import wF._
    
    // the initial condition
    val (x, Interval(low,high)) = A match {
      case Box(components) => components head
    }
    val x0 = acumen.interpreters.enclosure.solver.vero.Interval(low,high)
    val (t0,t) = T match {case Interval(l,h) => (l,h)}

    val degree = 2 // where could we get this from?
    val maxRad: java.math.BigDecimal  = read("1")// where could we get this from?

    // create an instance of TaylorSolver with the proper field and call the right function!
    object theSolver extends TaylorSolver{
      def field[A:Floating](t:A,x:A) = {
	val w = implicitly[Floating[A]]
	import w._
	fieldFor(e)(w)(t,x)
      }
    }

    val (acumen.interpreters.enclosure.solver.vero.Interval(vl,vh),
	 _ , 
	 acumen.interpreters.enclosure.solver.vero.Interval(bl,bh)) = theSolver.oneEnclosureWithRefinement[Real](degree)(maxRad)(x0)(t0)(t)
    
    println("[" + bl + ", " + bh + "]")
    
    (UnivariateAffineEnclosure(T, Box(x -> Interval(vl,vh)(Rounding(prec))))(Rounding(prec)), Box(x -> Interval(bl,bh)(Rounding(prec))))
  }
  
  def fieldFor[A:Floating](e : Expression) : (A,A) => A = {
    val w = implicitly[Floating[A]]
    import w._

    // for x' = f(x), x(0) = x0 we get x and f(x) : there is only one variable, we do not need it.
    (t:A,a:A) =>
      e match {
	case Constant(ival) => read((ival midpoint) toString)
	case Variable(x)    => a
	case Abs(d)         => abs(fieldFor(d)(w)(t,a))
	case Sqrt(d)        => sqrt(fieldFor(d)(w)(t,a))
	case Negate(d)      => negate(fieldFor(d)(w)(t,a))
	case Plus(d,f)      => fieldFor(d)(w)(t,a) + fieldFor(f)(w)(t,a)
	case Multiply(d,f)  => fieldFor(d)(w)(t,a) * fieldFor(f)(w)(t,a)
	case Divide(d,f)    => fieldFor(d)(w)(t,a) / fieldFor(f)(w)(t,a)
      }
  }

}

/*
   the general case in terms of the IVP and the underlying type. IVP
   with acceleration hardcoded, the eqns are
   x'' = f(t, x', x), x'(0) = x0', x(0) = x0
   and it is turned into (as used in the solver)
   y' = f(t,y,x) and x' = y with y(0) = x0' and x(0) = x0

   def field[A:Floating](t:A, y:A, x:A):A

   In terms of Jans rules this will be mapping y' to the expression
   for f. So I have to get the function (A will be Real, BigDecimal)
   asociated to the expression.  There is no t in the expressions.  
   \y -> \x -> decide whether to use y or x depending on whether what
   I see is the non-primed variable or another variable. We assume
   that we get the right kind of expression.

*/





/*
git status
git diff
git add path or file or -a
git commit -a
git push
*/

/*
I have written somewhere that what it returns is a UAE and a box.
A box seems to be a map of variable name to interval at a given time
A UAE seems to be a time interval and a box.
Be careful with the intervals, they are of another kind than the ones I use.
Jans intervals are with BigDecimal so I will have to use these in the solver.
*/


