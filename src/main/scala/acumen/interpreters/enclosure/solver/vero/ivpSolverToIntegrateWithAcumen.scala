package  acumen.interpreters.enclosure.solver.vero
/*

   In order to integrate with Acumen we provide a function that
   returns the whole blue-red box where the solution is enclosed and the
   interval at the end point where the next enclosure can start (the
   initial condition for the next time step).
  
   We also provide a function that produces a sequence of these pairs
   for the whole time interval.

   The driver will try to find a box for the whole time interval (for
   a max acceptable radius) and calculate the endtime interval with
   the help of taylor. Taylor can be used with more and more terms
   until the interval does not improve any more.  Division in 2 is
   done if either we cannot find a box or the endtime interval is not
   good enough. What I do not understand is how we can propagate
   backwards what good enough is at the endpoint of the first time
   interval. Do we use the same value? 

*/
trait TaylorSolver {
  // the general case in terms of the IVP and the underlying type
  def field[A:Floating](t:A,x:A):A
  
  // find a  box enclosing the solutions in T
  // the boxes go from blue low to red up in the x axis and on T in the time axis

  def plBox[A](rMax : A)
  (x0 : A)(t0 : A)(t : A)
  (implicit wF : Floating[A], wRN : RoundedNum[A])
  : Option[(Interval[A],Interval[A])] =
    {
      // calculate a box around x0 in the time interval dt if imposible:
      // None
      
      import Interval._
      import wF._
      import Dif._
      implicit val wFi = Interval.rn2floating(wRN)
      implicit val wFdi = a2dif(wFi)    
      import Rational._
      
      var dt     : A           = t - t0
      // this might not be a good choice when using spliting because we start with a big dt! Might start too big!
      // find a better way to do this: a parameter that can be chosen for the field? As we have rMax we could have rMin.
      // try this in the tests we do for Jan and Adam.
      // Or maybe it is the other way round: for big time intervals start with big radies!!
      var x0ival : Interval[A] = lift(x0) + Interval(fromInteger(0),dt)*field(Interval(t0,t0+dt),lift(x0)) 
      var rad    : A           = x0ival size
      
      x0ival = Interval(x0-rad,x0+rad)
      var term   : Interval[A] = lift(x0) + Interval(fromInteger(0),dt)*field(Interval(t0,t0+dt),x0ival) 
      
      def includes(x:Interval[A],y:Interval[A]):Boolean =
	wRN.compare(x.l,y.l)<= 0 && wRN.compare(x.u,y.u)>=0

      // Danis' factor
      val radIncFactor : A = fromDouble(1.1)
      while(wRN.compare(rad,rMax)<0 && !includes(x0ival,term)){
	rad = rad * radIncFactor
	x0ival = Interval(x0-rad,x0+rad)
	term = lift(x0) + Interval(fromInteger(0),dt)*field(Interval(t0,t0+dt),x0ival);
      }
      
      if (includes(x0ival,term)) Some((x0ival,Interval(t0,t)))
      else None
    }



  /*
  * In case the driver is something else and we just get to compute one enclosure with refinment
  * */
  def oneEnclosureWithRefinement[A]
  (n : Int) // the taylor degree
  (rMax : A)
  (x0 : Interval[A])(t0 : A)(t : A)
  (implicit wF : Floating[A], wRN : RoundedNum[A]) 
  : 
  (Interval[A],Interval[A],Interval[A]) // an enclosure as a rectangle xXt and the reduced interval at the end time
  = {
    import Interval._
    import wF._
    import Dif._
    implicit val wFi = Interval.rn2floating(wRN)
    implicit val wFdi = a2dif(wFi)    
    import Rational._
    
    var dt : A = t - t0

    (plBox(rMax)(x0.l)(t0)(t), plBox(rMax)(x0.u)(t0)(t)) match {
      case (None,_)  => sys.error("could not find a box around x0 low in the taylor solver")
      case (_,None)  => sys.error("could not find a box around x0 high in the taylor solver")
      case (Some((blue,_)),Some((red,_))) => {
	val epi = oneStepSolver(t0,x0.u,red,x0.l,blue)(n)(t) // n is the taylor degree
	// println(epi)
	(Interval(blue.l,red.u),
	 Interval(t0,t),
	 epi)
      }
    }
  }
  

  
  def oneStepSolver[A](t0:A, xu : A, redx:Interval[A], xl: A, bluex:Interval[A])
  (n:Int)
  (t:A)
  (implicit wF : Floating[A], wRN : RoundedNum[A])
  :Interval[A] = {
    val top    = Interval.lift(xTaylor[A](t0,xu)(n)(t)) + L(t0,redx)(n+1)(t)
    val bottom = Interval.lift(xTaylor[A](t0,xl)(n)(t)) + L(t0,bluex)(n+1)(t)
    Interval(bottom.l, top.u)
  }
  
  def xTaylor[A:Floating]
  (t0:A,x0:A)
  (n:Int)
  (t:A)
  :A = 
    {
      val w = implicitly[Floating[A]]
      import w._
      import Dif._
      implicit val wFda = a2dif(w)
      def h : Dif[A] = field(dVar(t0),x)
      def x : Dif[A] = D(x0,h)
      val dt = t-t0
      var y : Dif[A] = x
      var f : A = fromInteger(1)
      var v : A = fromInteger(1)
      var p : A = x0
      for (k <- 1 to n){
	f = f * fromInteger(k)
	v = v * dt
	y = dif(y)
	p = p + value(y)*v/f
      }
      p
    } 
  
  
  def L[A](t0:A,x0:Interval[A])
  (n:Int)
  (t:A)
  (implicit wF : Floating[A], wRN : RoundedNum[A])
  :Interval[A] = 
    {
      import wF._
      import wRN._
      import Dif._
      implicit val wFi = Interval.rn2floating(wRN)
      implicit val wFdi = a2dif(wFi)
      val tival : Interval[A] = Interval(t0,t)

      def h : Dif[Interval[A]] = field(dVar(tival),x)
      def x : Dif[Interval[A]] = D(x0,h)

      val dt : Interval[A]= Interval(fromInteger(0),t-t0)
      var y : Dif[Interval[A]] = x
      var f : A = fromInteger(1) // factorials are just numbers turned into intervals when needed
      var v : Interval[A] = wFi.fromInteger(1)
      var p : Interval[A] = x0
      for (k <- 1 to n){
	f = f * fromInteger(k)
	v = v * dt
	y = dif(y)
	
      }
      value(y)*v/(Interval.lift(f))
    }
}

