package  acumen.interpreters.enclosure.solver.vero
// to classify the type of endpoints in intervals
// includes the ops we will need in our examples

// Jan's implementations
import java.math.BigDecimal;
trait RoundedNum[A]{
  implicit def fromIntegerU(x:BigInt):A 
  implicit def fromIntegerD(x:BigInt):A
  implicit def fromIntegerU(x:Int):A 
  implicit def fromIntegerD(x:Int):A
  def plusU(x:A,y:A):A 
  def plusD(x:A,y:A):A 
  def timesU(x:A,y:A):A 
  def timesD(x:A,y:A):A 
  def minusU(x:A,y:A):A 
  def minusD(x:A,y:A):A
  def negateU(x:A):A
  def negateD(x:A):A
  def absU(x:A):A
  def absD(x:A):A
  def signumU(x:A):A
  def signumD(x:A):A
  def divU(x:A,y:A):A // = timesU(x,recipU(y))
  def divD(x:A,y:A):A // = timesD(x,recipD(y))
  def recipU(x:A):A // = divU(fromIntegerU(1),x)
  def recipD(x:A):A // = divD(fromIntegerD(1),x)
  def readU(s:String):A
  def readD(s:String):A
  def compare(x:A,b:A):Int 
  def powU(x:A,n:Int):A
  def powD(x:A,n:Int):A
  def sqrtU(x:A):A
  def sqrtD(x:A):A
  def piU:A
  def piD:A
}

object RoundedNum{
  def max[A](x:A,y:A)(implicit w : RoundedNum[A]):A=
    if(w.compare(x,y)<=0)y
    else x

  def min[A](x:A,y:A)(implicit w : RoundedNum[A]):A=
    if(w.compare(x,y)<=0)x
    else y
}


class Context(prec:Int) {
 private val mcU:java.math.MathContext = 
   new java.math.MathContext(prec,java.math.RoundingMode.CEILING)
 private val mcD:java.math.MathContext = 
   new java.math.MathContext(prec,java.math.RoundingMode.FLOOR)

 implicit object BigDecimal2RoundedNum extends RoundedNum[BigDecimal] {
   def fromIntegerU(x:BigInt):BigDecimal = readU(x.toString())
   def fromIntegerD(x:BigInt):BigDecimal = readD(x.toString())
   def fromIntegerU(x:Int):BigDecimal = readU(x.toString())
   def fromIntegerD(x:Int):BigDecimal = readD(x.toString())
   def plusU(x:BigDecimal,y:BigDecimal):BigDecimal = x.add(y,mcU)
   def plusD(x:BigDecimal,y:BigDecimal):BigDecimal = x.add(y,mcD)
   def timesU(x:BigDecimal,y:BigDecimal):BigDecimal = x.multiply(y,mcU)
   def timesD(x:BigDecimal,y:BigDecimal):BigDecimal = x.multiply(y,mcD)
   def minusU(x:BigDecimal,y:BigDecimal):BigDecimal = x.subtract(y,mcU)
   def minusD(x:BigDecimal,y:BigDecimal):BigDecimal = x.subtract(y,mcD)
   def negateU(x:BigDecimal):BigDecimal = x.negate()
   def negateD(x:BigDecimal):BigDecimal = x.negate()
   def absU(x:BigDecimal):BigDecimal = x.abs()
   def absD(x:BigDecimal):BigDecimal = x.abs()
   def signumU(x:BigDecimal):BigDecimal = java.math.BigDecimal.valueOf(x.signum())
   def signumD(x:BigDecimal):BigDecimal = java.math.BigDecimal.valueOf(x.signum())
   def divU(x:BigDecimal,y:BigDecimal):BigDecimal = x.divide(y,mcU)
   def divD(x:BigDecimal,y:BigDecimal):BigDecimal = x.divide(y,mcD)
   def recipU(x:BigDecimal):BigDecimal = divU(java.math.BigDecimal.ONE,x)
   def recipD(x:BigDecimal):BigDecimal = divD(java.math.BigDecimal.ONE,x)
   def readU(s:String):BigDecimal = new java.math.BigDecimal(s,mcU)
   def readD(s:String):BigDecimal = new java.math.BigDecimal(s,mcD)      
   def compare(x:BigDecimal,y:BigDecimal):Int = x.compareTo(y)
   def powU(x:BigDecimal,n:Int):BigDecimal = x.pow(n,mcU)
   def powD(x:BigDecimal,n:Int):BigDecimal = x.pow(n,mcD)
   
   private def readMc(s:String,mc:java.math.MathContext):BigDecimal = 
     new java.math.BigDecimal(s,mc)
   // rewite sqrtD and sqrtU in terms of sqrtRnd taking a rounding mode
   def sqrtMc(x:BigDecimal,mc:java.math.MathContext):BigDecimal = {
     val half = readMc("0.5",mc)
     var res = x 
     var tmp = readMc("0",mc) 
     while (res.compareTo(tmp) != 0) {
       tmp = res
       res = x.divide(tmp,mc).add(tmp,mc).multiply(half,mc)
     }
     res
   }      
   def sqrtU(x:BigDecimal):BigDecimal = sqrtMc(x,mcU)
   def sqrtD(x:BigDecimal):BigDecimal = sqrtMc(x,mcD)
   /* from http://en.wikipedia.org/wiki/%CE%A0 */
   def piHelper:BigDecimal = {
     val rm = java.math.RoundingMode.CEILING
     // adding 10 to prec gives exact rounding at least up to prec 10000
     val mc = new java.math.MathContext(prec+10,rm)
     def read(s:String):BigDecimal = readMc(s,mc)
     val two = read("2")
     val four = read("4")
     var a = read("1")
     var b = sqrtMc(read("0.5"),mc)
     var p = read("1")
     var t = read("0.25")
     var aPrev = read("0")
     var bPrev = read("0")
     var pPrev = read("0")
     var tPrev = read("0")
     while (a.compareTo(b) != 0) {
       aPrev = a
       bPrev = b
       pPrev = p
       tPrev = t
       a = aPrev.add(bPrev,mc).divide(two,mc)
       b = sqrtMc(aPrev.multiply(bPrev,mc),mc)
       p = two.multiply(pPrev,mc)
       t = tPrev.subtract(aPrev.subtract(a,mc).pow(2,mc).multiply(pPrev,mc),mc)
     }
     a.add(b,mc).pow(2,mc).divide(four.multiply(t,mc),mc)
   }
   def piU:BigDecimal = piHelper.round(mcU)
   def piD:BigDecimal = piHelper.round(mcD)
 }
}

object TestBigDec extends App {
  def f[A](x:A)(implicit w:RoundedNum[A]) = { 
    import w._
    piU
  }
  implicit val w = new Context(args(0).toInt).BigDecimal2RoundedNum
  import w._
  val x = readU("2")
  println(piD)
  println(piU)
  println(f(piD))
  println(divU(x,readU("3")))
}

