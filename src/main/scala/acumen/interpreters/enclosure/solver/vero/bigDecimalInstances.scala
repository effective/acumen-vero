package  acumen.interpreters.enclosure.solver.vero
import org.nevec.rjm._
import java.math.BigDecimal

/*
Needs to be revised in case we have to parameterize with mathcontext
* Well, it has, at least for division!
  */
object BigDecimalInstances{
  
  trait BigDecimalIsNum extends Num[BigDecimal]{
    def plus(x:BigDecimal,y:BigDecimal):BigDecimal= x add y
    def minus(x:BigDecimal,y:BigDecimal):BigDecimal= x subtract y
    def times(x:BigDecimal,y:BigDecimal):BigDecimal= x multiply y
    def negate(x:BigDecimal):BigDecimal =  x negate
    def abs(x:BigDecimal): BigDecimal = x abs
    def signum(x:BigDecimal):BigDecimal = new BigDecimal(x signum)
    implicit def fromInteger(x:BigInt):BigDecimal = new BigDecimal(x toString)
    implicit def fromInteger(x:Int):BigDecimal = new BigDecimal(x toString)
  }

  implicit object BigDecimal2Num extends BigDecimalIsNum
  
  trait BigDecimalIsFractional extends Fractional[BigDecimal] with BigDecimalIsNum{
    // when this is used in operations that are not related to intervals, where the context is set,
    // we need to provide either a context or a scale or both to divide so that we do not risk an arith exception
    // otherwise it would just be x divide y
    // CHECK: is this the right way of choosing the MC?
    override def div(x:BigDecimal,y:BigDecimal):BigDecimal = x.divide(y,new java.math.MathContext( 2+x.precision() )) 

    implicit def fromRational(r:Rational):BigDecimal = 
      new BigDecimal(Rational.numerator(r) toString) divide new BigDecimal(Rational.denominator(r) toString)
  }

  implicit object BigDecimal2Fractional extends BigDecimalIsFractional

  trait BigDecimalIsFloating extends Floating[BigDecimal] with BigDecimalIsFractional{
    import org.nevec.rjm._
    def pi : BigDecimal = BigDecimalMath.PI
    def exp(x:BigDecimal):BigDecimal = BigDecimalMath.exp(x)
    def sqrt(x:BigDecimal):BigDecimal= BigDecimalMath.sqrt(x)
    def log(x:BigDecimal):BigDecimal= BigDecimalMath.log(x)
    def pow(x:BigDecimal,y:BigDecimal):BigDecimal= BigDecimalMath.pow(x,y)
    def logBase(x:BigDecimal,y:BigDecimal):BigDecimal= log(y)/log(x)
    def sin(x:BigDecimal):BigDecimal= BigDecimalMath.sin(x)
    def tan(x:BigDecimal):BigDecimal= BigDecimalMath.tan(x)
    def cos(x:BigDecimal):BigDecimal= BigDecimalMath.cos(x)
    def asin(x:BigDecimal):BigDecimal= BigDecimalMath.asin(x)
    def atan(x:BigDecimal):BigDecimal= BigDecimalMath.atan(x)
    def acos(x:BigDecimal):BigDecimal= BigDecimalMath.acos(x)
    def sinh(x:BigDecimal):BigDecimal= BigDecimalMath.sinh(x)
    def tanh(x:BigDecimal):BigDecimal= BigDecimalMath.tanh(x)
    def cosh(x:BigDecimal):BigDecimal= BigDecimalMath.cosh(x)
    def asinh(x:BigDecimal):BigDecimal= BigDecimalMath.asinh(x)
    def atanh(x:BigDecimal):BigDecimal=log(x+sqrt(x*x-BigDecimal.ONE))
    def acosh(x:BigDecimal):BigDecimal= BigDecimalMath.acosh(x)
    def doubleValue(x:BigDecimal):Double= x doubleValue
    def show(x:BigDecimal, t:Int):Unit= print(x toString)
    def read(x:String) : BigDecimal = new BigDecimal(x)
  }
  
  implicit object BigDecimal2Floating extends BigDecimalIsFloating
  
}
