package  acumen.interpreters.enclosure.solver.vero
// copying what I think are the escential parts of  Stream
import Dif.D
abstract class  Dif[+A]{
  def value : A
  def df : Dif[A]
  def isEmpty : Boolean
  protected def dfDefined : Boolean // related to the part I don't understand for thread safe.
}

final class D[A](x:A,d: =>Dif[A]) extends Dif[A]{
  override def value = x
  override def isEmpty = false
  // this is the thing for thread safe that I did not understand yet
  @volatile private[this] var dfVal: Dif[A] = _
  def dfDefined: Boolean = dfVal ne null
  override def df: Dif[A] = {
    if (!dfDefined)
      synchronized {
	if (!dfDefined) dfVal = d
      }
    dfVal
  }

  override def toString():String = value.toString()
  
  override def equals(other:Any) : Boolean = other match {
    case that  : D[A] => 
      that match {case D(v,_) => v==value}
  }
}

object Dif {
  // to avoid using new (apply) and for pattern matching (unapply)
  object D{ 
    def apply[A](x:A,d: =>Dif[A]) = new D(x,d)
    def unapply[A](xd: Dif[A]):Option[(A,Dif[A])]=
      if (xd.isEmpty) None
      else Some((xd.value, xd.df))
  }

  def value[A](v: =>Dif[A]) = v.value 
  def dif[A](v: =>Dif[A]) = v.df 
  def dVar[A](x:A)(implicit m:Num[A]) = D(x,a2dif(m).fromInteger(1)) // see below for a2dif
  trait DifisOrdering[A] extends Ordering[Dif[A]]{
    implicit val a : Ordering[A]
    def compare(x:Dif[A], y:Dif[A]):Int =  (x,y) match {
      case (D(vx,dx),D(vy,dy)) => a.compare(vx,vy)
    }
  }

  trait DifDoubleIsOrdering extends DifisOrdering[Double] {
    implicit val a = Ordering.Double
  }
  implicit object DifDouble2Ordering extends DifDoubleIsOrdering

  // instantiating Num and the rest of the hierarchy
  trait DifIsNum[A] extends Num[Dif[A]]{
    implicit val a : Num[A]
    // import a._
    
    // why cannot I use +, -, * for A in x.value + y.value? Seems to be related to 2.10
    def plus(x: Dif[A], y: Dif[A]): Dif[A] = D(a.plus(x.value, y.value), plus(x.df,y.df))

    def minus(x: Dif[A], y: Dif[A]): Dif[A] = D(a.minus(x.value,y.value), minus(x.df,y.df))


    // ***************************
    // be careful: times is exponential, 2 recursive function calls!!!!!!
    // think of binomials and convolution terms
    // in Haskell:
    // pascal = iterate (\row -> zipWith (+) ([0] ++ row) (row ++ [0])) [1]

    // cList u @ (x:xs) v @ (y:ys) = 
    //   x*y : [sum $ zipWith3 (\a b c -> a*b*c) (take i u) (reverse (take i v))(pascal!!(i-1)) | i <- [2 .. ]]

    // ***************************

    def times(x: Dif[A], y: Dif[A]): Dif[A] = {
      D(a.times(x.value,y.value),plus(times(x.df,y),times(x,y.df)))
    }

    def negate(x: Dif[A]): Dif[A] = minus(fromInteger(0), x)

    implicit def fromInteger(x: BigInt): Dif[A] = D(a.fromInteger(x),fromInteger(0))
    implicit def fromInteger(x:Int):Dif[A] = D(a.fromInteger(x),fromInteger(0))

    override def abs(x: Dif[A]) : Dif[A] = D(a.abs(x.value),signum(times(x,x.df)))
    override def signum(x: Dif[A]) : Dif[A] = D(a.signum(x.value),fromInteger(0))
  }

  trait DifDoubleIsNum extends DifIsNum[Double] {
    implicit val a = Num.Double2Num
  }
  implicit object DifDouble2Num extends DifDoubleIsNum

  // Fractional
  trait DifIsFractional[A] extends Fractional[Dif[A]] with DifIsNum[A]{
    implicit val a : Fractional[A]
    import a._
    override def recip(x:Dif[A]):Dif[A]= {
      def ip : Dif[A] = D (a.recip(x.value),times(negate(x.df),times(ip,ip)))
      ip
    }
      
    implicit def fromRational(r:Rational):Dif[A]= D(a.fromRational(r),fromInteger(0))
  }
  
  trait DifDoubleIsFractional extends DifIsFractional[Double]{
    implicit val a = Fractional.Double2Fractional
  }
  
  implicit object DifDouble2Fractional extends DifDoubleIsFractional

  trait DifIsFloating[A] extends Floating[Dif[A]] with DifIsFractional[A]{
    implicit val a : Floating[A]
    import a._

    def lift(fs:Stream[A=>A],p:Dif[A]):Dif[A] = fs match {
      case f #:: fds => D(f(p.value),times(p.df,lift(fds,p)))
    }

    def cycle[T](seq: Seq[T]):Stream[T] = Stream.continually(seq.toStream).flatten

    def o[T1,T2,T3](f:T2=>T3,g:T1=>T2):T1=>T3= x=> f(g(x))

    def pi : Dif[A] = D(a.pi,fromInteger(0))
    def exp(x:Dif[A]):Dif[A] = {
      def r: Dif[A] = D (a.exp(x.value),times(x.df,r))
      r
    }
    def sqrt(x:Dif[A]):Dif[A] = {
      def r : Dif[A] = D(a.sqrt(x.value), div(x.df,times(fromInteger(2),r)))
      r
    }
    def log(x:Dif[A]):Dif[A] = D(a.log(x.value), div(x.df, x))

    def pow(x:Dif[A],y:Dif[A]):Dif[A] = exp(times(log(x),y))
    def logBase(x:Dif[A],y:Dif[A]):Dif[A] = div(log(y),log(x))

    def sin(x:Dif[A]):Dif[A] = lift (cycle(Seq(a.sin _,a.cos _, 
					       o(a.negate _,a.sin _), 
					       o(a.negate _, a.cos _))),
				     x)
    def tan(x:Dif[A]):Dif[A] = div(sin(x),cos(x))

    def cos(x:Dif[A]):Dif[A] =lift (cycle(Seq(a.cos _,
					      o(a.negate _,a.sin _), 
					      o(a.negate _, a.cos _),
					      a.sin _)),
				    x)

    def asin(x:Dif[A]):Dif[A] = D(a.asin(x.value),div(x.df,sqrt(minus(fromInteger(1),times(x,x)))))

    def atan(x:Dif[A]):Dif[A] = D(a.atan(x.value), div(x.df,sqrt(minus(times(x,x),fromInteger(1)))))
    def acos(x:Dif[A]):Dif[A] = D(a.acos(x.value), div(negate(x.df),sqrt(minus(fromInteger(1),times(x,x)))))
    def sinh(x:Dif[A]):Dif[A] = div(minus(exp(x),exp(negate(x))),fromInteger(2))
    def tanh(x:Dif[A]):Dif[A] = div(sinh(x),cosh(x))
    def cosh(x:Dif[A]):Dif[A] = div(plus(exp(x),exp(negate(x))),fromInteger(2))
    def asinh(x:Dif[A]):Dif[A] = log(plus(x,sqrt(plus(times(x,x),fromInteger(1))))) 
    def atanh(x:Dif[A]):Dif[A] = div(minus(log(plus(fromInteger(1),x)),log(minus(fromInteger(1),x))),fromInteger(2))
    def acosh(x:Dif[A]):Dif[A]= log(plus(x,sqrt(minus(times(x,x),fromInteger(1))))) 
    def doubleValue(x:Dif[A]):Double = a.doubleValue(value(x)) 
    def show(x:Dif[A], t:Int):Unit = a.show(value(x),t)
    def read(x:String) : Dif[A] = D(a.read(x),fromInteger(0))
    
  }

  trait DifDoubleIsFloating extends DifIsFloating[Double]{
    implicit val a = Floating.Double2Floating
  }
  
  implicit object DifDouble2Floating extends DifDoubleIsFloating

  def a2dif[A](m:Num[A]):Num[Dif[A]] = new DifIsNum[A]{implicit val a=m}
  def a2dif[A](m:Fractional[A]):Fractional[Dif[A]] = new DifIsFractional[A]{implicit val a=m}
  def a2dif[A](m:Floating[A]):Floating[Dif[A]] = new DifIsFloating[A]{implicit val a=m}
  

  
}


