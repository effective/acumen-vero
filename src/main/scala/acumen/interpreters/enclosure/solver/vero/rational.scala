package  acumen.interpreters.enclosure.solver.vero
case class Rational(n: BigInt, d: BigInt) {
      require(d != 0)
      private val g = gcd(n.abs, d.abs)
      val numer = (if (d < 0) -n else n) / g
      val denom = d.abs / g

  
      // general, object related
      override def equals(other: Any): Boolean =
        other match {
          case that: Rational =>
            (that canEqual this) &&
            numer == that.numer &&
            denom == that.denom
          case _ => false
        }
  
      def canEqual(other: Any): Boolean =
        other.isInstanceOf[Rational]
  
      override def hashCode: Int =
        41 * (
          41 + numer.hashCode
        ) + denom.hashCode
  
      override def toString =
        if (denom == 1) numer.toString else numer +"\\"+ denom

      // operations
      def + (that : Rational) : Rational = 
	new Rational(numer * that.denom + that.numer * denom, denom * that.denom)

      def - (that : Rational) : Rational = 
	new Rational(numer * that.denom - that.numer * denom, denom * that.denom)

      def * (that : Rational) : Rational = 
	new Rational(numer *  that.numer, denom * that.denom)
  
      def / (that : Rational) : Rational =
	new Rational(numer*that.denom,denom*that.numer)
  
      def < (that : Rational) : Boolean = numer * that.denom < that.numer * denom
      def <= (that : Rational) : Boolean = numer * that.denom <=  that.numer * denom
      def > (that : Rational) : Boolean = numer * that.denom > that.numer * denom
      

      // auxiliary
      private def gcd(a: BigInt, b: BigInt): BigInt =
        if (b == 0) a else gcd(b, a % b)
}

object Rational {

  class BigIntPair2Rational(x:BigInt){
    def \ (y:BigInt) : Rational = new Rational(x,y)
  }

  implicit def bip2rat(x:BigInt) = new BigIntPair2Rational(x)

  class IntPair2Rational(x:Int){
    def \ (y:BigInt) : Rational = new Rational(x,y)
  }

  implicit def ip2rat(x:Int) = new IntPair2Rational(x)
  
  implicit def fromDouble(x:Double):Rational={
    fromDouble(x toString)
  }

  implicit def fromDouble(x:String):Rational={
    // to do this trick we need to take care of other cases of double literals!!
    val int = x.takeWhile(c=>c!='.')
    val decimals = x.stripPrefix(int+".")
    val ln:Int = decimals.length
    val den:BigInt = BigInt(10).pow(ln)
    val num:BigInt = BigInt(int+decimals)
    Rational(num,den)
  }
  
  def numerator(r:Rational)   = r match{case Rational(n,d)=>n}
  def denominator(r:Rational) = r match{case Rational(n,d)=>d}

  // provide instances of Num and Fractional

  def min(x:Rational, y:Rational) =
    if (x > y) y else x
  
  def max(x:Rational, y:Rational) =
    if (x < y) y else x

  def abs(x:Rational) : Rational = new Rational(x.numer abs,x.denom)
}
