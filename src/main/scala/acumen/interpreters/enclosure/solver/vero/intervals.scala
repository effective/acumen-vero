
/* Author:  Walid Taha, with help from Paul Brauner,  Veronica Gaspes and Jan Duracz.
   Definitions from "W. Tucker, Auto-validating numerical methods".
*/
/*
New version: parameterized over the type of endpoints using Jan Duracz rounded numbers
*/

// TODO: enforce  the endpoints are l < u
// TODO: factory + private constructor

package  acumen.interpreters.enclosure.solver.vero
import RoundedNum._
import Interval._
case class Interval[A](l:A, u:A)(implicit w : RoundedNum[A]){
  import w._
  
  def size () : A = minusU(u,l) // OBS: att göra: return interval 
  def midPoint () : A = divD(plusU(l,u),twoD) // what  U, what D? // OBS, att göra!
  
  def + (that : Interval[A]) =
    Interval(plusD(this.l,that.l), plusU(this.u,that.u))

  def - (that : Interval[A]) =
    Interval(minusD(this.l,that.u), minusU(this.u,that.l))

  def ^ (exp:Int) : Interval[A] = {
    val mig = if (compare(l,zeroD)<=0 && compare(zeroU,u)<=0) zeroD
	      else min(absD(l),absD(u)) // obs rd!
    val mag = max(absU(l),absU(u)) // obs rd!
    if(exp==0) iOne
    else if(exp > 0 && exp % 2 == 1) Interval(powD(l,exp), powU(u,exp))
    else if(exp > 0 && exp % 2 == 0) Interval(powD(mig,exp), powU(mag,exp))
    else if ((compare(l,zeroD)<=0 && compare(zeroU,u)<=0)) (this invert) ^ exp
    else sys.error("Interval.Value.pow problem")
  }

  def invert () = // should check non-zero
    Interval (divD(oneD,this.u), divU(oneU,this.l))
  
  def / (that : Interval[A]) : Interval[A] = this * (that.invert)
  
  def * (that : Interval[A]) : Interval[A] = {
    val prodsD : List[A]= 
	 List(timesD(l, that.u), timesD (u,that.l), timesD (u,that.u))
       val prodsU : List[A] = 
	 List(timesU (l,that.u), timesU (u,that.l), timesU (u,that.u))
       Interval(prodsD.foldLeft(timesD (l,that.l))(min(_, _)),
	     prodsU.foldLeft(timesU (l,that.l))(max(_, _)))
   }
  
  override def toString() = "[" + l + ", " + u + "]"
}

object Interval{
  def zeroD[A](implicit w : RoundedNum[A]) = w.fromIntegerD(0)
  def zeroU[A](implicit w : RoundedNum[A]) = w.fromIntegerU(0)
  def oneD[A](implicit w : RoundedNum[A])  = w.fromIntegerD(1)
  def oneU[A](implicit w : RoundedNum[A])  = w.fromIntegerU(1)
  def twoD[A](implicit w : RoundedNum[A])  = w.fromIntegerD(2)
  def twoU[A](implicit w : RoundedNum[A])  = w.fromIntegerU(2)  
  def iZero[A:RoundedNum] = Interval(zeroD,zeroU) 
  def iOne[A:RoundedNum]  = Interval(oneD, oneU)
  def lift[A:RoundedNum] (a : A) : Interval[A] =  Interval(a,a)
  def lift[A] (a : BigInt)(implicit w : RoundedNum[A]) : Interval[A] =  Interval(w.fromIntegerD(a),w.fromIntegerU(a))
  def lift[A] (a : Int)(implicit w : RoundedNum[A]) : Interval[A] =  Interval(w.fromIntegerD(a),w.fromIntegerU(a))

  trait IntervalIsNum[A] extends Num[Interval[A]]{
    implicit val w : RoundedNum[A]
    import w._
    def plus(x:Interval[A],y:Interval[A]) : Interval[A] = x+y
    def times(x:Interval[A],y:Interval[A]):Interval[A] = x*y
    def minus(x:Interval[A],y:Interval[A]):Interval[A]= x-y
    def negate(x:Interval[A]):Interval[A]= iZero-x
    def abs(x:Interval[A]):Interval[A] = sys.error("") // Jan lägger till den
    def signum(x:Interval[A]):Interval[A] = sys.error("") // Jan lägger till den
    override def pow(x:Interval[A],n:Int) = x^n
    implicit def fromInteger(x:BigInt):Interval[A] = lift(x)
    implicit def fromInteger(x:Int):Interval[A] = lift(x)
  }
  
  
  trait IntervalIsFractional[A] extends Fractional[Interval[A]] with IntervalIsNum[A]{
    override def div(x:Interval[A],y:Interval[A]):Interval[A] = x/y
    
    implicit def fromRational(r:Rational):Interval[A] = {
      fromInteger(Rational.numerator(r)) / fromInteger(Rational.denominator(r))
    } // Jan: could be made tighter!
    
  }
  
  
  trait IntervalIsFloating[A] extends Floating[Interval[A]] with IntervalIsFractional[A]{
    def pi : Interval[A]= Interval(w.piD,w.piU)
    def exp(x:Interval[A]):Interval[A]= sys.error("")
    def sqrt(x:Interval[A]):Interval[A]= Interval(w.sqrtD(x.l),w.sqrtU(x.u))
    def log(x:Interval[A]):Interval[A]= sys.error("")
    def pow(x:Interval[A],y:Interval[A]):Interval[A]= sys.error("")
    def logBase(x:Interval[A],y:Interval[A]):Interval[A]= sys.error("")
    def sin(x:Interval[A]):Interval[A]= sys.error("sin")
    def tan(x:Interval[A]):Interval[A]= sys.error("")
    def cos(x:Interval[A]):Interval[A]= sys.error("")
    def asin(x:Interval[A]):Interval[A]= sys.error("")
    def atan(x:Interval[A]):Interval[A]= sys.error("")
    def acos(x:Interval[A]):Interval[A]= sys.error("")
    def sinh(x:Interval[A]):Interval[A]= sys.error("")
    def tanh(x:Interval[A]):Interval[A]= sys.error("")
    def cosh(x:Interval[A]):Interval[A]= sys.error("")
    def asinh(x:Interval[A]):Interval[A]= sys.error("")
    def atanh(x:Interval[A]):Interval[A]= sys.error("")
    def acosh(x:Interval[A]):Interval[A]= sys.error("")
    def doubleValue(x:Interval[A]):Double= sys.error("")
    def show(x:Interval[A], t:Int):Unit= print(x)
    def read(x:String) : Interval[A] = Interval(w.readD(x),w.readU(x))
  }
 
  def rn2num[A](implicit rn : RoundedNum[A]):Num[Interval[A]] = new IntervalIsNum[A]{val w = rn}
  def rn2fractional[A](implicit rn : RoundedNum[A]):Fractional[Interval[A]] = new IntervalIsFractional[A]{val w = rn}
  def rn2floating[A](implicit rn : RoundedNum[A]):Floating[Interval[A]] = new IntervalIsFloating[A]{val w = rn}
  
  def intervalBigDecimal2floating(prec : Int) : Floating[Interval[java.math.BigDecimal]] = {
    rn2floating(new Context(prec).BigDecimal2RoundedNum)
  }

  /*
  import com.hp.creals._
  implicit val IntervalCR2Floating : Floating[Interval[CR]] = rn2floating(CRinstances.CR2RoundedNum)
*/

  trait DifIntervalIsFloating[A] extends Dif.DifIsFloating[Interval[A]]{
    implicit val a : Floating[Interval[A]]
    override def pow(x:Dif[Interval[A]],n:Int):Dif[Interval[A]] = {
	if(n==0)Dif.D(a.fromInteger(1),fromInteger(0))
	else Dif.D(x.value^n, fromInteger(n)*pow(x,n-1)*x.df)
    }
  }
  
  implicit def DifInterval2Floating(prec:Int) : DifIntervalIsFloating[java.math.BigDecimal] = 
    new DifIntervalIsFloating[java.math.BigDecimal]{val a = intervalBigDecimal2floating(prec)}

  /*
  implicit val DifIntervalCR2Floating :  DifIntervalIsFloating[CR] = 
    new DifIntervalIsFloating[CR]{val a = IntervalCR2Floating}
*/
}
  
