package  acumen.interpreters.enclosure.solver.vero
import com.hp.creals._

object CRinstances{

  trait CRisNum extends Num[CR]{
    def plus(x:CR,y:CR):CR = x add y
    def minus(x:CR,y:CR):CR = x subtract y
    def times(x:CR,y:CR):CR=x multiply y
    def negate(x:CR):CR = x.negate()
    def abs(x:CR):CR = x.abs()
    def signum(x:CR):CR = CR.valueOf(x.signum)
    implicit def fromInteger(x:BigInt):CR = CR.valueOf(x.bigInteger)
    implicit def fromInteger(x:Int):CR = CR.valueOf(x)
  }
  implicit object CR2Num extends CRisNum

  trait CRisFractional extends Fractional[CR] with CRisNum{
    override def div(x:CR,y:CR):CR = x divide y
    implicit def fromRational(r:Rational):CR = {
      import Rational._
      CR.valueOf(numerator(r).bigInteger)  divide CR.valueOf(denominator(r).bigInteger)
    }
  }
  implicit object CR2Fractional extends CRisFractional
  
  trait CRisFloating extends Floating[CR] with  CRisFractional{
    def pi : CR = CR.PI
    def exp(x:CR):CR = x exp
    def sqrt(x:CR):CR = x sqrt
    def log(x:CR):CR = x ln
    def pow(x:CR,y:CR):CR = ((x ln) multiply y) exp 
    def logBase(x:CR,y:CR):CR= (y ln) / (x ln)
    def sin(x:CR):CR = x sin
    def tan(x:CR):CR= (x sin) / (x cos)
    def cos(x:CR):CR= x cos
    def asin(x:CR):CR = UnaryCRFunction.asinFunction.execute(x)
    def atan(x:CR):CR = UnaryCRFunction.atanFunction.execute(x)
    def acos(x:CR):CR = UnaryCRFunction.acosFunction.execute(x)
    def sinh(x:CR):CR = ((x exp) subtract (x.negate() exp))/ fromInteger(2) 
    def tanh(x:CR):CR= sinh(x) / cosh(x)
    def cosh(x:CR):CR=((x exp) add (x.negate() exp))/ fromInteger(2) 
    def asinh(x:CR):CR= (x add ((x multiply x add fromInteger(1)) sqrt)) ln
    def atanh(x:CR):CR=  (((fromInteger(1) add x) divide (fromInteger(1) subtract x)) ln) divide fromInteger(2)
    def acosh(x:CR):CR= (x add ((x multiply x subtract fromInteger(1)) sqrt)) ln
    def doubleValue(x:CR):Double  = x doubleValue
    def show(x:CR, t:Int):Unit = print(x toString t)
    def read(x:String):CR = CR.valueOf(x, 10) // radix is the base?
  }
  implicit object CR2Floating extends CRisFloating 

  
  // missing: diff CR and rounded number

}


object TestCR extends App {
  import CRinstances._
  import CR2Floating._
  val x = read(args(0))
  show(x, 20);println
  show(pi, 20);println
}

