package  acumen.interpreters.enclosure.solver.vero
case class Complex[A:Floating](re : A, im : A) {
  // general, object related
  override def equals(other: Any): Boolean =
    other match {
      case that: Complex[A] =>
        (that canEqual this) &&
      re == that.re &&
      im == that.im
      case _ => false
    }
  
  def canEqual(other: Any): Boolean =
    other.isInstanceOf[Complex[A]]
  
  override def hashCode: Int =
    41 * (
      41 + re.hashCode
    ) + im.hashCode
  
  override def toString =
    if (im == 0) re.toString 
    else if(re == 0) im.toString +"i" 
    else re.toString + " + " + im + "i"
  // operations
  val a = implicitly[Floating[A]]
  import a._
  
  def + (that : Complex[A]) : Complex[A] = 
    new Complex(re + that.re, im + that.im)
  
  def - (that : Complex[A]) : Complex[A] = 
    new Complex(re - that.re, im - that.im)
  
  def * (that : Complex[A]) : Complex[A] = 
    new Complex(re * that.re - im * that.im, re * that.im  + im * that.re)
  
  def / (that : Complex[A]) : Complex[A] = {
    val n = that.re * that.re + that.im * that.im
    new Complex((re * that.re + im * that.im)/n, (im * that.re - re * that.im)/n )
    
  }
}

object Complex {

  implicit def fromA[A:Floating](x:A):Complex[A]={
    val a = implicitly[Floating[A]]
    import a._
    Complex(x,0)
  }

  def i[A : Floating] : Complex[A] = {
    val a = implicitly[Floating[A]]
    import a._
    Complex(0,1)
  }
  
  def magnitude[A:Floating](z : Complex[A]) : A = {
    val a = implicitly[Floating[A]]
    import a._
    sqrt(z.re*z.re + z.im*z.im)
  }

  def cis[A:Floating](theta : A) : Complex[A] = {
    val a = implicitly[Floating[A]]
    import a._
    Complex(cos(theta),sin(theta))
  }
 
}


object TestComplex extends App{
  import Complex._
  val c : Complex[Double] = 1.0
  val i = Complex.i[Double]
  println(i*i)
  println(c + i)
  println(2.0 + i*3.0)
  val x : Double = 1.0
  val y : Double = 1.0
  val z = new Complex(x,y)
  println(z + z)
}

object C1 extends App{
  def power[A:Floating](x:A, n:Int) : A = {
    val a = implicitly[Floating[A]]
    import a._
    
    var p : A = 1
    for(i <- 1 to n)
      p = p * x
    p
  }
    
  import Complex._
  
  for(n <- 0 to (args(0) toInt))
    println(power(i[Double],n))
}
