package  acumen.interpreters.enclosure.solver.vero
trait Floating[A] extends Fractional[A] {
  def pi : A
  def exp(x:A):A
  def sqrt(x:A):A
  def log(x:A):A
  def pow(x:A,y:A):A
  def logBase(x:A,y:A):A
  def sin(x:A):A
  def tan(x:A):A
  def cos(x:A):A
  def asin(x:A):A
  def atan(x:A):A
  def acos(x:A):A
  def sinh(x:A):A
  def tanh(x:A):A
  def cosh(x:A):A
  def asinh(x:A):A
  def atanh(x:A):A
  def acosh(x:A):A
  def doubleValue(x:A) : Double
  def show(x:A, t:Int) : Unit
  def read(x:String) : A
  
  class FloatingOps(lhs: A) extends FractionalOps(lhs) {
    def **(rhs: A) = pow(lhs, rhs)
  }
  
  override implicit def mkNumericOps(lhs: A): FloatingOps = new FloatingOps(lhs)

}


object Floating{
  trait DoubleIsFloating extends Floating[Double] with  Fractional.DoubleIsFractional{
    def pi : Double = math.Pi
    def exp(x:Double):Double = math.exp(x)
    def sqrt(x:Double):Double = math.sqrt(x)
    def log(x:Double):Double = math.log(x)
    def pow(x:Double,y:Double):Double = math.pow(x,y)
    def logBase(x:Double,y:Double):Double=log(y)/log(x)
    def sin(x:Double):Double = math.sin(x)
    def tan(x:Double):Double=math.tan(x)
    def cos(x:Double):Double=math.cos(x)
    def asin(x:Double):Double=math.asin(x)
    def atan(x:Double):Double=math.atan(x)
    def acos(x:Double):Double=math.acos(x)
    def sinh(x:Double):Double=math.sinh(x)
    def tanh(x:Double):Double=math.tanh(x)
    def cosh(x:Double):Double=math.cosh(x)
    def asinh(x:Double):Double=log(x+sqrt(x*x+1))
    def atanh(x:Double):Double=log(x+sqrt(x*x-1))
    def acosh(x:Double):Double= log((1+x)/(1-x))/2
    def doubleValue(x:Double):Double = x 
    def show(x:Double,t:Int):Unit = print(x toString)
    def read(x:String) : Double = x toDouble
  }

  implicit object Double2Floating extends DoubleIsFloating

  trait ComplexIsFloating[A] extends Floating[Complex[A]] with  Fractional.ComplexIsFractional[A]{
    implicit val a : Floating[A]
    import Complex._ 

    def pi : Complex[A] = a.pi
    def exp(x:Complex[A]):Complex[A] = {
      val expx = a.exp(x.re)
      expx*a.cos(x.im) + expx*i*a.sin(x.im)
    }

    def sqrt(x:Complex[A]):Complex[A] = i 
      
    def log(x:Complex[A]):Complex[A] = i
    def pow(x:Complex[A],y:Complex[A]):Complex[A] = i
    def logBase(x:Complex[A],y:Complex[A]):Complex[A] = i
    def sin(x:Complex[A]):Complex[A]   = a.sin(x.re) * a.cosh(x.im) + i*a.cos(x.re)*a.sinh(x.im)
    def tan(x:Complex[A]):Complex[A]   = i
    def cos(x:Complex[A]):Complex[A]   = a.cos(x.re) * a.cosh(x.im) - i*a.sin(x.re)*a.sinh(x.im)
    def asin(x:Complex[A]):Complex[A]  = i
    def atan(x:Complex[A]):Complex[A]  = i
    def acos(x:Complex[A]):Complex[A]  = i
    def sinh(x:Complex[A]):Complex[A]  = a.cos(x.im)*a.sinh(x.re) + i*a.sin(x.im)*a.cosh(x.re)
    def tanh(x:Complex[A]):Complex[A]  = i
    def cosh(x:Complex[A]):Complex[A]  = a.cos(x.im)*a.cosh(x.re) + i*a.sin(x.im)*a.sinh(x.re)
    def asinh(x:Complex[A]):Complex[A] = i
    def atanh(x:Complex[A]):Complex[A] = i
    def acosh(x:Complex[A]):Complex[A] = i 
    def doubleValue(x:Complex[A]) :  Double = 0.1 // redo!
    def show(x:Complex[A],t:Int):Unit = print(x toString)
    def read(x:String) : Complex[A] = i // redo
  }

  trait ComplexDoubleIsFloating extends ComplexIsFloating[Double]{
    implicit val a = Floating.Double2Floating
  }
  
  implicit object ComplexDouble2Floating extends ComplexDoubleIsFloating
  /*
  import com.hp.creals._
  trait ComplexCRIsFloating extends ComplexIsFloating[CR]{
    implicit val a = CRinstances.CR2Floating
  }
  
  implicit object ComplexCR2Floating extends ComplexCRIsFloating
  */
}

