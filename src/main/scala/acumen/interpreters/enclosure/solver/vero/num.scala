package  acumen.interpreters.enclosure.solver.vero
trait Num[A]{
  def plus(x:A,y:A):A
  def times(x:A,y:A):A
  def minus(x:A,y:A):A
  def negate(x:A):A
  def abs(x:A):A
  def signum(x:A):A
  def pow(x:A, n:Int):A = {
    List.fill(n)(x).foldLeft(fromInteger(1))(times(_,_))
  }

  implicit def fromInteger(x:BigInt):A
  implicit def fromInteger(x:Int):A
  
  class Ops(lhs: A) {
    def +(rhs: A) = plus(lhs, rhs)
    def *(rhs: A) = times(lhs, rhs)
    def -(rhs: A) = minus(lhs, rhs)
  }
  
  implicit def mkNumericOps(lhs: A): Ops = new Ops(lhs)
  
}

object Num{
  
  // the instances
  trait IntIsNum extends Num[Int]{
    def plus(x:Int,y:Int):Int=x+y
    def minus(x:Int,y:Int):Int=x-y
    def times(x:Int,y:Int):Int=x*y
    def negate(x:Int):Int = 0-x
    def abs(x:Int):Int = math.abs(x)
    def signum(x:Int):Int = if(x>0)1 else if(x<0)-1 else 0
    implicit def fromInteger(x:BigInt):Int = x.intValue()
    implicit def fromInteger(x:Int):Int = x
  }
  implicit object Int2Num extends IntIsNum

  trait BigIntIsNum extends Num[BigInt]{
    def plus(x:BigInt,y:BigInt):BigInt=x+y
    def minus(x:BigInt,y:BigInt):BigInt=x-y
    def times(x:BigInt,y:BigInt):BigInt=x*y
    def negate(x:BigInt):BigInt = 0-x
    def abs(x:BigInt):BigInt = x abs
    def signum(x:BigInt):BigInt = if(x>0)1 else if(x<0)-1 else 0
    implicit def fromInteger(x:BigInt):BigInt = x
    implicit def fromInteger(x:Int):BigInt = x
  }
  implicit object BigInt2Num extends BigIntIsNum


  trait DoubleIsNum extends Num[Double]{
    def plus(x:Double,y:Double):Double=x+y
    def minus(x:Double,y:Double):Double=x-y
    def times(x:Double,y:Double):Double=x*y
    def negate(x:Double):Double = 0-x
    def abs(x:Double):Double = math.abs(x)
    def signum(x:Double):Double = if(x>0)1 else if(x<0)-1 else 0
    implicit def fromInteger(x:BigInt):Double = x.doubleValue()
    implicit def fromInteger(x:Int):Double = x.doubleValue()

  }
  implicit object Double2Num extends DoubleIsNum

  // generic instances that help us use the predefined functions in Scala that use Numeric
  /*
   implicit def numIsNumeric[A:Num] = new Numeric[A]{
   val a = implicitly[Num[A]]
   import a._
   
   def compare(x:A,y:A):Int={
   val sg = a.signum(x-y)
   if(sg==fromInteger(0))0
   else if(sg==fromInteger(1))1
   else -1
    }
    
    def plus(x:A,y:A):A=x+y
    def minus(x:A,y:A):A=x-y
    def times(x:A,y:A):A=x*y
    def negate(x:A):A=a.negate(x)
    def fromInt(x:Int):A=fromInteger(x)
    def toInt(x:A):Int=sys.error("toInt")
    def toLong(x:A):Long=sys.error("toLong")
    def toFloat(x:A):Float=sys.error("toFloat")
    def toDouble(x:A):Double=sys.error("toDouble")
  }
*/
  trait RationalIsNum extends Num[Rational]{
    import Rational._
    def plus(x:Rational,y:Rational):Rational=x+y
    def minus(x:Rational,y:Rational):Rational=x-y
    def times(x:Rational,y:Rational):Rational=x*y
    def negate(x:Rational):Rational = 0-x
    def abs(x:Rational): Rational = Rational.abs(x)
    def signum(x:Rational):Rational = if (x>0) 1 else if(x<0)-1 else 0
    implicit def fromInteger(x:BigInt):Rational = x \ 1
    implicit def fromInteger(x:Int):Rational = BigInt(x) \ 1
  }
  implicit object Rational2Num extends RationalIsNum

  trait ComplexIsNum[A] extends Num[Complex[A]]{
    implicit val a : Floating[A]
    import a._

    import Complex._
    def plus(x:Complex[A],y:Complex[A]):Complex[A] = x + y
    def minus(x:Complex[A],y:Complex[A]):Complex[A] = x - y
    def times(x:Complex[A],y:Complex[A]):Complex[A] = x * y
    def negate(x:Complex[A]):Complex[A] = minus(fromInteger(0),x)
    def abs(x:Complex[A]): Complex[A]   = magnitude(x)
    def signum(x:Complex[A]):Complex[A] = if(x == 0) fromInteger(0) else {
      val r = magnitude(x)
      new Complex(x.re/r, x.im/r)
    }
    implicit def fromInteger(x:BigInt):Complex[A] = a.fromInteger(x) 
    implicit def fromInteger(x:Int):Complex[A] = a.fromInteger(x) 
  }

  implicit def a2complex[A](m:Floating[A]):Num[Complex[A]] = new ComplexIsNum[A]{implicit val a=m}
  implicit val double2complex:Num[Complex[Double]] = a2complex(Floating.Double2Floating)
}

