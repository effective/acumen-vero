package  acumen.interpreters.enclosure.solver.vero
trait Fractional[A] extends Num[A]{
  def div(x:A,y:A):A = x*recip(y)
  def recip(x:A):A = div(fromInteger(1),x)
  implicit def fromRational(r:Rational):A

  class FractionalOps(lhs: A) extends Ops(lhs) {
    def /(rhs: A) = div(lhs, rhs)
  }

  override implicit def mkNumericOps(lhs: A): FractionalOps =
    new FractionalOps(lhs)
}


object Fractional{
  trait DoubleIsFractional extends Fractional[Double] with Num.DoubleIsNum{
    override def div(x:Double,y:Double):Double = x / y
    implicit def fromRational(r:Rational):Double = {
      r.numer /  r.denom
    }
  }

  implicit object Double2Fractional extends DoubleIsFractional

  trait RationalIsFractional extends Fractional[Rational] with Num.RationalIsNum{
    override def div(x:Rational,y:Rational):Rational = x / y
    implicit def fromRational(r:Rational):Rational = r
  }
  implicit object Rational2Fractional extends RationalIsFractional

 trait ComplexIsFractional[A] extends Fractional[Complex[A]] with Num.ComplexIsNum[A]{
   implicit val a : Floating[A]
   import a._
   import Complex._
   override def div(x:Complex[A],y:Complex[A]) : Complex[A] = x / y
   implicit def fromRational(r:Rational) : Complex[A] = a.fromRational(r)
  }
  implicit def a2complex[A](m:Floating[A]):Fractional[Complex[A]] = 
    new ComplexIsFractional[A]{implicit val a=m}
}


